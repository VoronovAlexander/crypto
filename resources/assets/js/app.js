require('./bootstrap');

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';

import store from './store'
import App from './App'

import IndexPage from './Pages/IndexPage'
import SnapshotIndexPage from './Pages/Snapshots/SnapshotsIndex'
import SnapshotShowPage from './Pages/Snapshots/SnapshotsShow'
import SignupPage from './Pages/SignupPage'
import SigninPage from './Pages/SigninPage'

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);

axios.defaults.baseURL = '/api';

const routes = [
        {
            path: '/',
            name: 'Главная страница',
            component: IndexPage,
        },{
            path: '/snapshots',
            name: 'Снимки',
            component: SnapshotIndexPage,
            meta: {
                auth: true,
                authRedirect: '/login'
            }
        },{
            path: '/snapshots/:name',
            name: 'Снимок',
            component: SnapshotShowPage,
            meta: {
                auth: true,
                authRedirect: '/login'
            }
        },{
            path: '/register',
            name: 'Зарегистрироваться',
            component: SignupPage,
            meta: {
                auth: false,
            }
        },{
            path: '/login',
            name: 'Войти',
            component: SigninPage,
            meta: {
                auth: false,
            }
        }
    ]

const router = new VueRouter({
    base: '/',
    linkActiveClass: '',
    linkExactActiveClass: 'active',
    routes: routes
});

Vue.router = router;

App.router = Vue.router;

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});



const app = new Vue({
  el: '#app',
  store,
  components: { App },
  template: '<App/>'
})
