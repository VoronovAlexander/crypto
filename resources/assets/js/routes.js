import IndexPage from './Pages/IndexPage'
import SnapshotIndexPage from './Pages/Snapshots/SnapshotsIndex'
import SignupPage from './Pages/SignupPage'
import SigninPage from './Pages/SigninPage'

export default new [
        {
            path: '/',
            name: 'Главная страница',
            component: IndexPage,
            meta: {
                auth: false
            }
        },{
            path: '/snapshots',
            name: 'Снимки',
            component: SnapshotIndexPage,
            meta: {
                auth: true
            }
        },{
            path: '/signup',
            name: 'Зарегистрироваться',
            component: SignupPage,
            meta: {
                auth: false
            }
        },{
            path: '/signin',
            name: 'Войти',
            component: SigninPage,
            meta: {
                auth: false
            }
        }
    ]
