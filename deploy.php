<?php

namespace Deployer;



require 'recipe/common.php';
require 'recipe/laravel.php';

set('repository', 'https://VoronovAlexander@bitbucket.org/VoronovAlexander/crypto.git');

server('production', '82.146.37.228',22)
    ->user('crypto')
    ->password('lM3sewQj')
    // ->identityFile('~/.ssh/id_rsa')
    ->set('branch', 'master')
    ->set('deploy_path', '/var/www/crypto/data/www/crypto.mintrocket.ru/crypto')
    ;

// Общий файл для всех релизов
add('shared_files', [
    '.env'
]);

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('writable_mode', 'chmod');
set('writable_chmod_mode', '0777');

//  Права на запись в папочки
set('writable_dirs', [
  'storage',
  'vendor',
  'public/uploads'
]);

// Накатить все зависимости, включая dev
task('deploy:composer_update', function() {
    run('cd {{release_path}} && composer update');
});

// Сброс дампа
task('deploy:dump_autoload', function() {
    run('cd {{release_path}} && composer dump-autoload');
});
// Выполнить миграции
task('deploy:migrate', function() {
    run('cd {{release_path}} && php artisan migrate');
});
// Выполнить миграции с применением сида
task('deploy:migrate:seed', function() {
    run('cd {{release_path}} && php artisan migrate --seed');
});
// Выполнить перезагразку миграций и применить сиды
task('deploy:migrate:refresh:seed', function() {
    run('cd {{release_path}} && php artisan migrate:refresh --seed');
});

task('deploy:access', function() {
  run('cd {{release_path}} && ( chmod -R 0777 ./storage ./storage/app storage/framework storage/framework/cache storage/framework/views storage/logs )');
});

task('deploy', [
  'deploy:prepare',
  'deploy:lock',
  'deploy:release',
  'deploy:update_code',
  'deploy:shared',
  'deploy:vendors',
  'deploy:writable',
  'deploy:access',
  'deploy:dump_autoload',
  'deploy:migrate',
  // 'deploy:migrate:seed',
  // 'deploy:migrate:refresh:seed',
  'artisan:cache:clear',
  'deploy:unlock',
])->desc('Deploy your project');
?>
