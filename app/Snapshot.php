<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Snapshot extends Model
{

    protected $fillable = [
        'name',
        'symbol',
        'rank',
        'price_usd',
        'price_btc',
        '24h_volume_usd',
        'market_cap_usd',
        'available_supply',
        'total_supply',
        'max_supply',
        'percent_change_1h',
        'percent_change_24h',
        'percent_change_7d',
        'last_updated',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
