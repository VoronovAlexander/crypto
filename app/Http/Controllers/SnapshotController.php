<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Snapshot;
class SnapshotController extends Controller
{
    function index() {

        $snapshots = Snapshot::orderBy('last_updated')->distinct('name')->paginate(15);
        // $snapshots = Snapshot::groupBy('name')
        //     ->orderBy('last_updated')
        //     ->distinct('name')
        //     ->paginate(15);

        return response([
            'status' => true,
            'data' => $snapshots
        ], 200);
    }

    function show($name) {

        $snapshot = Snapshot::where('name', $name)->first();

        return response([
            'status' => true,
            'data' => $snapshot
        ], 200);
    }

}
