<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Snapshot;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            // Запросим новые снимки
            $response = file_get_contents("https://api.coinmarketcap.com/v1/ticker/?limit=0");
            $snapshots = json_decode($response, true);

            // Если нет снимков - прервать
            if(count($snapshots) == 0) {
                return;
            }

            // Перебираем снимки
            foreach ($snapshots as $snapshot) {
                // Создадим
                unset($snapshot['id']);
                Snapshot::create($snapshot);
            }

        })->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
